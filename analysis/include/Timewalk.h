/* 
 * File:   Timewalk.h
 * Author: JW
 *
 * Created on 22. July 2014,
 */

#ifndef TIMEWALK_H
#define	TIMEWALK_H

#include <cmath>
#include "TBAnalysis.h"

class Timewalk: public TBAnalysis
{
private:
	std::map<std::pair<int, int>, TH1F*> h_delayHistMatched;
	std::map<std::pair<DUT*, DUT*>, TH1F*> h_DtHist;	
	std::map<std::pair<DUT*, DUT*>, TH1F*> h_timewalkHistMatched;
	std::map<std::pair<int, int>, std::map<DUT*, int>> m_L1collection;
	std::map<std::pair<DUT*, DUT*>, float> m_averageDel;
	std::map<std::pair<DUT*, DUT*>, int> m_trackCount;
	std::map<std::pair<DUT*, DUT*>, std::vector<int>> m_averageDt;
	
public:
	Timewalk()
	{
		name = "Timewalk";
	}
	virtual void init(const TBCore* core);
	virtual void event(const TBCore* core, const TBEvent* event);
	virtual void finalize(const TBCore* core);

};

// class factories
extern "C" TBAnalysis* create()
{
	return new Timewalk;
}

extern "C" void destroy(TBAnalysis* tba)
{
	delete tba;
}

#endif	/* TIMEWALK_H */

