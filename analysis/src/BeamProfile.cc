/* 
 * File:   BeamProfile.cc
 * Author: daniel
 * 
 * Created on 9. Februar 2014, 05:13
 */

#include "BeamProfile.h"

void BeamProfile::init(const TBCore* core)
{
	int iden;
	for(auto dut: core->usedDUT)
	{
		iden = dut->iden;
		double dutPitchX = dut->getDutPitchX();
		double dutPitchY = dut->getDutPitchY();
		int nCols = dut->getNcols();
		int nRows = dut->getNrows();

		// check plot size and bin size
		BeamProfile::h_beamProfile[iden] = new TH2D("", ";Track X [#mum];Track Y [#mum]", 200, -dutPitchX*0.5, dutPitchX*1.5, 200, -dutPitchY*0.5, dutPitchY*1.5);
		BeamProfile::h_beamProfileZoom[iden] = new TH2D("", ";Track X [#mum];Track Y [#mum]", 50, 0, dutPitchX, 50, 0, dutPitchY);
		BeamProfile::h_beamProfileMasked[iden] = new TH2D("", ";Track X [#mum];Track Y [#mum]", nCols, 0, dutPitchX, nRows, 0, dutPitchY);
		BeamProfile::h_dutHit[iden] = new TH2D("", ";Track X [#mum];Track Y [#mum]", 80, -500, dutPitchX+500, 80, -50, dutPitchX+50);
		BeamProfile::h_chi2[iden] = new TH1D("", ";Track #chi^{2}", 100, 0, 300);
		BeamProfile::h_chi2Zoom[iden] = new TH1D("", ";Track #chi^{2}", 100, 0, 30);
		BeamProfile::h_chi2ZoomMatchedTracks[iden] = new TH1D("", ";Track #chi^{2}", 100, 0, 30);

		BeamProfile::h_angle[iden] = new TH2D("", ";dx/dz;dy/dz", 400, -0.004, 0.004, 400, -0.004, 0.004);
		BeamProfile::h_angle_normalcuts[iden] = new TH2D("", ";dx/dz;dy/dz", 400, -0.004, 0.004, 400, -0.004, 0.004);
		BeamProfile::h_beamProfile_noAngleCut[iden] = (TH2D*) h_beamProfile[iden]->Clone(""); //Same binning etc.
		BeamProfile::h_TrackShift_XY[iden] = new TH2D("", ";x [#mum];y [#mum]", 250, -.5, .5, 250, -.5, .5);

		BeamProfile::h_corr_PosVsAngle_XX[iden] = new TH2D("", ";x [#mum];dx/dz", 400, -500, dutPitchX+500, 400, -0.004, 0.004);
		BeamProfile::h_corr_PosVsAngle_XY[iden] = new TH2D("", ";x [#mum];dy/dz", 400, -500, dutPitchX+500, 400, -0.004, 0.004);
		BeamProfile::h_corr_PosVsAngle_YX[iden] = new TH2D("", ";y [#mum];dx/dz", 400, -50, dutPitchY+50, 400, -0.004, 0.004);
		BeamProfile::h_corr_PosVsAngle_YY[iden] = new TH2D("", ";y [#mum];dy/dz", 400, -50, dutPitchY+50, 400, -0.004, 0.004);
	}
}

void BeamProfile::event(const TBCore* core, const TBEvent* event)
{
	int iden = event->iden;
	DUT* dut = event->dut;

	if(event->fRawTracks == kBad)
	{
		return;
	}
	
	// not sure, whether loop over all raw tracks or only matched tracks 
	for(auto track: event->rawTracks)
	{
		BeamProfile::h_chi2[iden]->Fill(track->chi2);
		BeamProfile::h_chi2Zoom[iden]->Fill(track->chi2);
		
		// DON'T check angleCuts, only Chi2
		if(track->fTrackChi2 == kGood)
		{
			BeamProfile::h_angle[iden]->Fill(track->dxdz, track->dydz);
			BeamProfile::h_beamProfile_noAngleCut[iden]->Fill(track->trackX, track->trackY);
			BeamProfile::h_corr_PosVsAngle_XX[iden]->Fill(track->trackX, track->dxdz);
			BeamProfile::h_corr_PosVsAngle_XY[iden]->Fill(track->trackX, track->dydz);
			BeamProfile::h_corr_PosVsAngle_YX[iden]->Fill(track->trackY, track->dxdz);
			BeamProfile::h_corr_PosVsAngle_YY[iden]->Fill(track->trackY, track->dydz);
		}
	}
	
	if(event->fTracks == kBad)
	{
		return;
	}

	for(auto track: event->tracks)
	{
		BeamProfile::h_chi2ZoomMatchedTracks[iden]->Fill(track->chi2);
		BeamProfile::h_beamProfile[iden]->Fill(track->trackX, track->trackY);
		BeamProfile::h_beamProfileZoom[iden]->Fill(track->trackX, track->trackY);
		BeamProfile::h_angle_normalcuts[iden]->Fill(track->dxdz, track->dydz);
		BeamProfile::h_TrackShift_XY[iden]->Fill(track->dxdz * track->zpos, track->dydz * track->zpos);
		BeamProfile::h_dutHit[iden]->Fill(track->trackX, track->trackY);

		if(track->fTrackEdgeRegion == kGood || track->fTrackCentralRegion == kGood)
		{
			BeamProfile::h_beamProfileMasked[iden]->Fill(track->trackX, track->trackY);
		}
	}	
}

void BeamProfile::finalize(const TBCore* core)
{
	core->output->processName = BeamProfile::name;
	
	int iden;
	char* histoTitle = new char[500];
	
	for(auto dut: core->usedDUT)
	{
		iden = dut->iden;

		// set up cuts
		core->output->cuts = "raw tracks";
		core->output->currentPreprocessCuts = "";
		// -----------
		
		std::sprintf(histoTitle, "#chi^{2} DUT %i", iden);
		BeamProfile::h_chi2[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "chi2_dut_%i", iden);
		BeamProfile::h_chi2[iden]->SetName(histoTitle);
		core->output->drawAndSave(BeamProfile::h_chi2[iden], "", "em");
	
		std::sprintf(histoTitle, "#chi^{2} Zoom DUT %i", iden);
		BeamProfile::h_chi2Zoom[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "chi2Zoom_dut_%i", iden);
		BeamProfile::h_chi2Zoom[iden]->SetName(histoTitle);
		core->output->drawAndSave(BeamProfile::h_chi2Zoom[iden], "", "em");
		
		
		// set up cuts
		core->output->cuts = "raw tracks with good chi2";
		core->output->currentPreprocessCuts = "";
		// -----------
		
		std::sprintf(histoTitle, "Angle DUT %i", iden);
		BeamProfile::h_angle[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "angle_dut_%i", iden);
		BeamProfile::h_angle[iden]->SetName(histoTitle);
		core->output->drawAndSave(BeamProfile::h_angle[iden], "colz", "e");

		std::sprintf(histoTitle, "Track shift on DUT %i", iden);
		BeamProfile::h_TrackShift_XY[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "trackShift_dut_%i", iden);
		BeamProfile::h_TrackShift_XY[iden]->SetName(histoTitle);
		core->output->drawAndSave(BeamProfile::h_TrackShift_XY[iden], "colz", "e");
		
		std::sprintf(histoTitle, "Beam profile no angle cut DUT %i", iden);
		BeamProfile::h_beamProfile_noAngleCut[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "beamprofile_noAngleCut_dut_%i", iden);
		BeamProfile::h_beamProfile_noAngleCut[iden]->SetName(histoTitle);
		//core->output->drawAndSave(BeamProfile::h_beamProfile_noAngleCut[iden], "cont4z", "e");
		core->output->drawAndSave(BeamProfile::h_beamProfile_noAngleCut[iden], "colz", "e");

		std::sprintf(histoTitle, "Corr pos Vs angle dx/dz DUT %i", iden);
		BeamProfile::h_corr_PosVsAngle_XX[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "corr_PosVsAngle_XX_dut_%i", iden);
		BeamProfile::h_corr_PosVsAngle_XX[iden]->SetName(histoTitle);
		core->output->drawAndSave(BeamProfile::h_corr_PosVsAngle_XX[iden], "colz", "e");
		
		std::sprintf(histoTitle, "Corr pos Vs angle dy/dz DUT %i", iden);
		BeamProfile::h_corr_PosVsAngle_XY[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "corr_PosVsAngle_XY_dut_%i", iden);
		BeamProfile::h_corr_PosVsAngle_XY[iden]->SetName(histoTitle);
		core->output->drawAndSave(BeamProfile::h_corr_PosVsAngle_XY[iden], "colz", "e");
	
		std::sprintf(histoTitle, "Corr pos Vs angle dx/dz DUT %i", iden);
		BeamProfile::h_corr_PosVsAngle_YX[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "corr_PosVsAngle_YX_dut_%i", iden);
		BeamProfile::h_corr_PosVsAngle_YX[iden]->SetName(histoTitle);
		core->output->drawAndSave(BeamProfile::h_corr_PosVsAngle_YX[iden], "colz", "e");
	
		std::sprintf(histoTitle, "Corr pos Vs angle dy/dz DUT %i", iden);
		BeamProfile::h_corr_PosVsAngle_YY[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "corr_PosVsAngle_YY_dut_%i", iden);
		BeamProfile::h_corr_PosVsAngle_YY[iden]->SetName(histoTitle);
		core->output->drawAndSave(BeamProfile::h_corr_PosVsAngle_YY[iden], "colz", "e");

		TH1D* h_angle_X = (TH1D*) BeamProfile::h_angle[iden]->ProjectionX(" ");
		std::sprintf(histoTitle, "Angle dx/dz DUT %i", iden);
		h_angle_X->SetTitle(histoTitle);
		std::sprintf(histoTitle, "angle_dxdz_dut_%i", iden);
		h_angle_X->SetName(histoTitle);
		core->output->drawAndSave(h_angle_X, "", "em");
		
		TH1D* h_angle_Y = (TH1D*) BeamProfile::h_angle[iden]->ProjectionY(" ");
		std::sprintf(histoTitle, "Angle dy/dz DUT %i", iden);
		h_angle_Y->SetTitle(histoTitle);
		std::sprintf(histoTitle, "angle_dydz_dut_%i", iden);
		h_angle_Y->SetName(histoTitle);
		core->output->drawAndSave(h_angle_Y, "", "em");

		TH1D* h_trackX_noAngleCut = (TH1D*) BeamProfile::h_beamProfile_noAngleCut[iden]->ProjectionX(" ");
		std::sprintf(histoTitle, "Track X no angle cut DUT %i", iden);
		h_trackX_noAngleCut->SetTitle(histoTitle);
		std::sprintf(histoTitle, "trackX_noAngleCut_dut_%i", iden);
		h_trackX_noAngleCut->SetName(histoTitle);
		core->output->drawAndSave(h_trackX_noAngleCut, "", "em");
		
		TH1D* h_trackY_noAngleCut = (TH1D*) BeamProfile::h_beamProfile_noAngleCut[iden]->ProjectionY(" ");
		std::sprintf(histoTitle, "Track Y no angle cut DUT %i", iden);
		h_trackY_noAngleCut->SetTitle(histoTitle);
		std::sprintf(histoTitle, "trackY_noAngleCut_dut_%i", iden);
		h_trackY_noAngleCut->SetName(histoTitle);
		core->output->drawAndSave(h_trackY_noAngleCut, "", "em");
		
		
		// set up cuts
		core->output->cuts = "good tracks";
		core->output->currentPreprocessCuts = "";
		// -----------
		
		std::sprintf(histoTitle, "Beam profile DUT %i", iden);
		BeamProfile::h_beamProfile[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "beamprofile_dut_%i", iden);
		//core->output->drawAndSave(BeamProfile::h_beamProfile[iden], "cont4z", "e");
		BeamProfile::h_beamProfile[iden]->SetName(histoTitle);
		core->output->drawAndSave(BeamProfile::h_beamProfile[iden], "colz", "e");
	
		
		std::sprintf(histoTitle, "Beam profile zoom DUT %i", iden);
		BeamProfile::h_beamProfileZoom[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "beamprofilezoom_dut_%i", iden);
		BeamProfile::h_beamProfileZoom[iden]->SetName(histoTitle);
		core->output->drawAndSave(BeamProfile::h_beamProfileZoom[iden], "colz", "e");
		
		std::sprintf(histoTitle, "Duthit DUT %i", iden);
		BeamProfile::h_dutHit[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "duthit_dut_%i", iden);
		BeamProfile::h_dutHit[iden]->SetName(histoTitle);
		//core->output->drawAndSave(BeamProfile::h_dutHit[iden], "cont4z", "e");
		core->output->drawAndSave(BeamProfile::h_dutHit[iden], "colz", "e");
		
		std::sprintf(histoTitle, "#chi^{2} Zoom Matched Tracks DUT %i", iden);
		BeamProfile::h_chi2ZoomMatchedTracks[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "chi2ZoomMatchedTracks_dut_%i", iden);
		BeamProfile::h_chi2ZoomMatchedTracks[iden]->SetName(histoTitle);
		core->output->drawAndSave(BeamProfile::h_chi2ZoomMatchedTracks[iden], "", "em");
		
		std::sprintf(histoTitle, "Angle normalcuts DUT %i", iden);
		BeamProfile::h_angle_normalcuts[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "angle_normalcuts_dut_%i", iden);
		BeamProfile::h_angle_normalcuts[iden]->SetName(histoTitle);
		core->output->drawAndSave(BeamProfile::h_angle_normalcuts[iden], "colz", "e");
	
		std::sprintf(histoTitle, "Beam profile masked DUT %i", iden);
		BeamProfile::h_beamProfileMasked[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "beamprofilemasked_dut_%i", iden);
		BeamProfile::h_beamProfileMasked[iden]->SetName(histoTitle);
		core->output->drawAndSave(BeamProfile::h_beamProfileMasked[iden], "colz", "e");
		
		TH1D* h_trackX = (TH1D*) BeamProfile::h_beamProfile[iden]->ProjectionX(" ");
		std::sprintf(histoTitle, "Track X DUT %i", iden);
		h_trackX->SetTitle(histoTitle);
		std::sprintf(histoTitle, "trackX_dut_%i", iden);
		h_trackX->SetName(histoTitle);
		core->output->drawAndSave(h_trackX, "", "em");
		
		
		TH1D* h_trackY = (TH1D*) BeamProfile::h_beamProfile[iden]->ProjectionY(" ");
		std::sprintf(histoTitle, "Track Y DUT %i", iden);
		h_trackY->SetTitle(histoTitle);
		std::sprintf(histoTitle, "trackY_dut_%i", iden);
		h_trackY->SetName(histoTitle);
		core->output->drawAndSave(h_trackY, "", "em");

		
		delete BeamProfile::h_beamProfile[iden];
		delete BeamProfile::h_beamProfileZoom[iden];
		delete BeamProfile::h_beamProfileMasked[iden];
		delete BeamProfile::h_chi2[iden];
		delete BeamProfile::h_chi2Zoom[iden];
		delete BeamProfile::h_chi2ZoomMatchedTracks[iden];
		delete BeamProfile::h_dutHit[iden];
		delete BeamProfile::h_angle[iden];
		delete BeamProfile::h_angle_normalcuts[iden];
		delete BeamProfile::h_beamProfile_noAngleCut[iden];
		delete BeamProfile::h_TrackShift_XY[iden];

		delete BeamProfile::h_corr_PosVsAngle_XX[iden];
		delete BeamProfile::h_corr_PosVsAngle_XY[iden];
		delete BeamProfile::h_corr_PosVsAngle_YX[iden];
		delete BeamProfile::h_corr_PosVsAngle_YY[iden];
		
		delete h_trackX;
		delete h_trackY;
		delete h_angle_X;
		delete h_angle_Y;
		delete h_trackX_noAngleCut;
		delete h_trackY_noAngleCut;
	}
	BeamProfile::h_beamProfile.clear();
	BeamProfile::h_beamProfileZoom.clear();
	BeamProfile::h_beamProfileMasked.clear();
	BeamProfile::h_chi2.clear();
	BeamProfile::h_chi2Zoom.clear();
	BeamProfile::h_chi2ZoomMatchedTracks.clear();
	BeamProfile::h_dutHit.clear();
	BeamProfile::h_angle.clear();
	BeamProfile::h_angle_normalcuts.clear();
	BeamProfile::h_beamProfile_noAngleCut.clear();
	BeamProfile::h_TrackShift_XY.clear();

	BeamProfile::h_corr_PosVsAngle_XX.clear();
	BeamProfile::h_corr_PosVsAngle_XY.clear();
	BeamProfile::h_corr_PosVsAngle_YX.clear();
	BeamProfile::h_corr_PosVsAngle_YY.clear();
	delete[] histoTitle;
}
