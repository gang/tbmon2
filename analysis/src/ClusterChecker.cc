/* 
 * File:   ClusterChecker.cc
 * Author: daniel
 * 
 * Created on 15. Februar 2014, 15:22
 */

#include "ClusterChecker.h"

void ClusterChecker::init(const TBCore* core)
{
	int iden;
	for(auto dut: core->usedDUT)
	{
		iden = dut->iden;

		int nCols = dut->getNcols();
	 	int nRows = dut->getNrows();
		
		ClusterChecker::h_clusterMult[iden] = new TH1D("", ";Cluster Multiplicity", 20, -.5, 19.5);
		ClusterChecker::h_nHits[iden] = new TH1D("", ";Number of Hits", 20, -.5, 19.5);
		ClusterChecker::h_matchClusterSize[iden] = new TH1D("", ";Matched Cluster Size", 20, -.5, 19.5);
		ClusterChecker::h_matchClusterSizeX[iden] = new TH1D("", ";Matched Cluster Size X", 20, -.5, 19.5);
		ClusterChecker::h_matchClusterSizeY[iden] = new TH1D("", ";Matched Cluster Size Y", 20, -.5, 19.5);
		ClusterChecker::h_unmatchClusterSize[iden] = new TH1D("", ";Un-Matched Cluster Size", 20, -.5, 19.5);
		ClusterChecker::h_matchClusterLvl1[iden] = new TH1D("", ";Matched Cluster Lvl1", 16, -.5, 15.5);
		ClusterChecker::h_matchClusterLvl1Map[iden] = new TH2D("", "Matched Cluster Lvl1 Map;Column;Row", nCols, -.5, nCols-.5, nRows, -.5, nRows-.5);
		ClusterChecker::h_matchClusterToTCenterMap[iden] = new TH2D("", "Matched Cluster ToT Center Map;Column;Row", nCols, -.5, nCols-.5, nRows, -.5, nRows-.5);
	}
}

void ClusterChecker::event(const TBCore* core, const TBEvent* event)
{
	if(event->fClusters != kGood)
	{
		return;
	}
	
	if(event->fTracks != kGood)
	{
		return;
	}

	DUT* dut = event->dut;
	int iden = event->iden;
	
	ClusterChecker::h_clusterMult[iden]->Fill(event->clusters.size());
	ClusterChecker::h_nHits[iden]->Fill(event->hits.size());

	std::vector<TBCluster*> matchedCluster;
	
	for(auto tbtrack: event->tracks)
	{
		if(tbtrack->fTrackMaskedRegion == kGood)
		{
			continue;
		}
		
		if(tbtrack->fMatchedCluster == kBad)
		{
			continue;
		}
		
		ClusterChecker::h_matchClusterSize[iden]->Fill(tbtrack->matchedCluster->hits.size());
		
		int clusterSizeX = TBCluster::spannedCols(tbtrack->matchedCluster, event);
		int clusterSizeY = TBCluster::spannedRows(tbtrack->matchedCluster, event);
		ClusterChecker::h_matchClusterSizeX[iden]->Fill(clusterSizeX);
		ClusterChecker::h_matchClusterSizeY[iden]->Fill(clusterSizeY);	
		
		TBHit* maxToTHit = TBCluster::getMaxToTCell(tbtrack->matchedCluster);
		ClusterChecker::h_matchClusterLvl1[iden]->Fill(maxToTHit->lv1);
		ClusterChecker::h_matchClusterLvl1Map[iden]->Fill(maxToTHit->col, maxToTHit->row, maxToTHit->lv1);
		ClusterChecker::h_matchClusterToTCenterMap[iden]->Fill(maxToTHit->col, maxToTHit->row);
		
		matchedCluster.push_back(tbtrack->matchedCluster);
	}
	
	for(auto tbcluster: event->clusters)
	{
		if(std::find(matchedCluster.begin(), matchedCluster.end(), tbcluster) == matchedCluster.end())
		{
			ClusterChecker::h_unmatchClusterSize[iden]->Fill(tbcluster->hits.size());
		}
	}
	
	matchedCluster.clear();
}

void ClusterChecker::finalize(const TBCore* core)
{
	core->output->processName = ClusterChecker::name;
	
	int iden;
	char* histoTitle = new char[500];
	for(auto dut: core->usedDUT)
	{
		iden = dut->iden;
		
		ClusterChecker::h_matchClusterLvl1Map[iden]->Divide(ClusterChecker::h_matchClusterLvl1Map[iden], ClusterChecker::h_matchClusterToTCenterMap[iden], 1.0, 1.0, "B");

		// set up cuts
		core->output->cuts = "good clusters";
		core->output->currentPreprocessCuts = "";
		// -----------
		
		std::sprintf(histoTitle, "Multiplicity DUT %i", iden);
		ClusterChecker::h_clusterMult[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "multiplicity_dut_%i", iden);
		ClusterChecker::h_clusterMult[iden]->SetName(histoTitle);
		core->output->drawAndSave(ClusterChecker::h_clusterMult[iden], "", "em");
		
		std::sprintf(histoTitle, "Pixel hits DUT %i", iden);
		ClusterChecker::h_nHits[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "pixelHits_dut_%i", iden);
		ClusterChecker::h_nHits[iden]->SetName(histoTitle);
		core->output->drawAndSave(ClusterChecker::h_nHits[iden], "", "em");
		
		// set up cuts
		core->output->cuts = "matched cluster without masked regions";
		core->output->currentPreprocessCuts = "";
		// -----------
		
		std::sprintf(histoTitle, "Match cluster size DUT %i", iden);
		ClusterChecker::h_matchClusterSize[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "matchClusterSize_dut_%i", iden);
		ClusterChecker::h_matchClusterSize[iden]->SetName(histoTitle);
		core->output->drawAndSave(ClusterChecker::h_matchClusterSize[iden], "", "em");
		
		std::sprintf(histoTitle, "Match cluster size col DUT %i", iden);
		ClusterChecker::h_matchClusterSizeX[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "matchClusterSizeCol_dut_%i", iden);
		ClusterChecker::h_matchClusterSizeX[iden]->SetName(histoTitle);
		core->output->drawAndSave(ClusterChecker::h_matchClusterSizeX[iden], "", "em");
	
		std::sprintf(histoTitle, "Match cluster size row DUT %i", iden);
		ClusterChecker::h_matchClusterSizeY[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "matchClusterSizeRow_dut_%i", iden);
		ClusterChecker::h_matchClusterSizeY[iden]->SetName(histoTitle);
		core->output->drawAndSave(ClusterChecker::h_matchClusterSizeY[iden], "", "em");
		
		std::sprintf(histoTitle, "Match cluster lv1 DUT %i", iden);
		ClusterChecker::h_matchClusterLvl1[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "matchClusterLv1_dut_%i", iden);
		ClusterChecker::h_matchClusterLvl1[iden]->SetName(histoTitle);
		core->output->drawAndSave(ClusterChecker::h_matchClusterLvl1[iden], "", "em");
		
		std::sprintf(histoTitle, "Match cluster lv1 map DUT %i", iden);
		ClusterChecker::h_matchClusterLvl1Map[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "matchClusterLv1Map_dut_%i", iden);
		ClusterChecker::h_matchClusterLvl1Map[iden]->SetName(histoTitle);
		core->output->drawAndSave(ClusterChecker::h_matchClusterLvl1Map[iden], "colz", "e");
		
		std::sprintf(histoTitle, "Match cluster ToT center map DUT %i", iden);
		ClusterChecker::h_matchClusterToTCenterMap[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "matchClusterToTCenterMap_dut_%i", iden);
		ClusterChecker::h_matchClusterToTCenterMap[iden]->SetName(histoTitle);
		core->output->drawAndSave(ClusterChecker::h_matchClusterToTCenterMap[iden], "colz", "e");
		
		// set up cuts
		core->output->cuts = "good unmatched clusters";
		core->output->currentPreprocessCuts = "";
		// -----------
		
		std::sprintf(histoTitle, "Unmatch cluster size DUT %i", iden);
		ClusterChecker::h_unmatchClusterSize[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "unmatchClusterSize_dut_%i", iden);
		ClusterChecker::h_unmatchClusterSize[iden]->SetName(histoTitle);
		core->output->drawAndSave(ClusterChecker::h_unmatchClusterSize[iden], "", "em");
		
		delete ClusterChecker::h_clusterMult[iden];
		delete ClusterChecker::h_nHits[iden];
		delete ClusterChecker::h_matchClusterSize[iden];
		delete ClusterChecker::h_matchClusterSizeX[iden];
		delete ClusterChecker::h_matchClusterSizeY[iden];
		delete ClusterChecker::h_unmatchClusterSize[iden];
		delete ClusterChecker::h_matchClusterLvl1[iden];
		delete ClusterChecker::h_matchClusterLvl1Map[iden];
		delete ClusterChecker::h_matchClusterToTCenterMap[iden];
	}
	ClusterChecker::h_clusterMult.clear();
	ClusterChecker::h_nHits.clear();
	ClusterChecker::h_matchClusterSize.clear();
	ClusterChecker::h_matchClusterSizeX.clear();
	ClusterChecker::h_matchClusterSizeY.clear();
	ClusterChecker::h_unmatchClusterSize.clear();
	ClusterChecker::h_matchClusterLvl1.clear();
	ClusterChecker::h_matchClusterLvl1Map.clear();
	ClusterChecker::h_matchClusterToTCenterMap.clear();
	delete[] histoTitle;
}

