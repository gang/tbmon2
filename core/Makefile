# Makefile for core and style files

CORE_HEADER_PATH := include/
CORE_SRC_PATH := src/
STYLE_HEADER_PATH := ../style/include/
STYLE_SRC_PATH := ../style/src/
BUILD_PATH := .build/


#DICT_HEADER := TBmonTMacroText.h
DICT_HEADER_DEF := $(patsubst %.h, %LinkDef.h, $(DICT_HEADER))
DICT_SRC := $(patsubst %.h, $(CORE_SRC_PATH)%Dict.cc, $(DICT_HEADER))
DICT_OBJ := $(patsubst %.h, $(BUILD_PATH)%Dict.o, $(DICT_HEADER))
CORE_FILE := $(wildcard $(CORE_SRC_PATH)*.cc)
CORE_OBJ := $(patsubst $(CORE_SRC_PATH)%.cc, $(BUILD_PATH)%.o, $(CORE_FILE))
STYLE_FILE := $(wildcard $(STYLE_SRC_PATH)*.cc)
STYLE_OBJ := $(patsubst $(STYLE_SRC_PATH)%.cc, $(BUILD_PATH)%.o, $(STYLE_FILE))

ECHO := echo
RM := rm
MKDIR := mkdir
MV := mv

CPP := c++
CPPFLAGS := -ldl -g -Werror

CERN_ROOT_FLAGS := $(shell root-config --cflags)
CERN_ROOT_LINK := $(shell root-config --glibs)
TBMON2_INCLUDE := -I$(CORE_HEADER_PATH) -I$(STYLE_HEADER_PATH)


.PHONY: build rebuild cleanall cleandict

.SUFFIXES: .o .cc

build: $(DICT_SRC) $(CORE_OBJ) $(STYLE_OBJ) 
	@if [ -d $(BUILD_PATH) ]; \
	then \
		$(ECHO) "Compilation done."; \
	else \
		$(ECHO) "Nothing to do."; \
	fi
	
rebuild: cleanall build 
	
$(BUILD_PATH)%.o: $(CORE_SRC_PATH)%.cc $(CORE_HEADER_PATH)%.h
	@if ! [ -d $(BUILD_PATH) ]; \
	then \
		$(MKDIR) -p $(BUILD_PATH);\
	fi
	$(CPP) $(CPPFLAGS) $(CERN_ROOT_FLAGS) $(TBMON2_INCLUDE) -std=c++11 -c -MMD -MP -MF $@.d -o $@ $< 
	
$(BUILD_PATH)%.o: $(STYLE_SRC_PATH)%.cc $(STYLE_HEADER_PATH)%.h
	@if ! [ -d $(BUILD_PATH) ]; \
	then \
		$(MKDIR) -p $(BUILD_PATH);\
	fi
	$(CPP) $(CPPFLAGS) $(CERN_ROOT_FLAGS) $(TBMON2_INCLUDE) -std=c++11 -c -MMD -MP -MF $@.d -o $@ $< 
	
$(BUILD_PATH)%Dict.o: $(CORE_SRC_PATH)%Dict.cc $(CORE_HEADER_PATH)%Dict.h
	@if ! [ -d $(BUILD_PATH) ]; \
	then \
		$(MKDIR) -p $(BUILD_PATH);\
	fi
	$(CPP) $(CPPFLAGS) $(CERN_ROOT_FLAGS) $(TBMON2_INCLUDE) -std=c++11 -c -MMD -MP -MF $@.d -o $@ $<
	
$(CORE_SRC_PATH)%Dict.cc: $(CORE_HEADER_PATH)%.h $(CORE_HEADER_PATH)%LinkDef.h 
	rootcint -v4 -f $*Dict.cc -c $(TBMON2_INCLUDE) $*.h $*LinkDef.h
	$(MV) *Dict.h $(CORE_HEADER_PATH)
	$(MV) *Dict.cc $(CORE_SRC_PATH) 
		
cleanall:
	@if [ -d $(BUILD_PATH) ]; \
	then \
		$(RM) -r $(BUILD_PATH); \
		$(ECHO) "Clean up done."; \
	else \
		$(ECHO) "Nothing to do."; \
	fi
	
cleandict:
	@if [ $(shell ls $(CORE_SRC_PATH) | grep ".*\Dict.cc" | wc -l) != "0" ]; \
	then \
		$(RM) $(DICT_SRC); \
	fi
	@if [ $(shell ls $(CORE_HEADER_PATH) | grep ".*\Dict.h" | wc -l) != "0" ]; \
	then \
		$(RM) $(CORE_HEADER_PATH)*Dict.h; \
	fi

