/* 
 * File:   TBOutput.cpp
 * 
 * Created on 5. Mai 2013
 */

#include "TBOutput.h"

TBOutput::TBOutput(TBCore* core)
{
	TBOutput::core = core;
	//TBOutput::storeevent = NULL;
}


/*void TBOutput::saveAndDraw(const std::string processName)
{
	TBOutput::core->tOutFile->cd();
}*/


void TBOutput::saveToFile(const std::string analysisName, const char* histoName, TNamed* histo) const{
	TBOutput::core->tOutFile->cd();
	char* fullName = new char[500];
	if(TBOutput::core->tbconfig->organizeOutput)
	{
		if(!TBOutput::core->tOutFile->GetDirectory(analysisName.c_str()))
		{
			TBOutput::core->tOutFile->mkdir(analysisName.c_str());
		}
	  TBOutput::core->tOutFile->cd(analysisName.c_str());
	  sprintf(fullName, "%s", histoName);
	}
	else{
	  sprintf(fullName, "%s-%s", analysisName.c_str(), histoName);
	}
	for(int ii =0; ii<500; ii++){ // Histo not callable from the cint cli if '-' in the name
		if( fullName[ii] == '-') { fullName[ii]='_'; }
		if( fullName[ii] == '\0') { break; }
	}
	
	/*TCanvas* canv = new TCanvas();
	canv->cd();
	
	TPad* pad1 = new TPad("", "p1", 0.05, 0, 0.95, 0.14);
	TPad* pad2 = new TPad("", "p2", 0, 0.15, 1, 1);
	
	pad1->Draw();
	pad1->SetFillColor(11);
	
	pad2->Draw();
	pad2->cd();
	histo->DrawClone("colz");*/
		
	//TPad* pad = new TPad("","p1",0,0,1,1);
	//pad->cd();
	//histo->Draw();
	//pad->Write(fullName); 
	
	//TBOutput::core->tOutFile->cd();
	//canv->Write();
	
	histo->SetName(fullName);
	histo->Write(fullName);
	
	 
	TBOutput::core->tOutFile->cd();
	delete [] fullName;
	
	//delete canvas;
	//delete pad2;
}

void TBOutput::setTitle( const std::string analysisName, const char* histoName, TH1* histo) const{
	char hTitle[600];
	sprintf(hTitle, "%s-%s-%s", core->tbconfig->rawDataName.c_str(), analysisName.c_str(),
			histoName);
	histo->SetTitle(hTitle);
}

void TBOutput::drawAndSave(const std::string analysisName, const char* histoName, TNamed* histo) const{
	drawToFile(analysisName.c_str(), histoName, histo);
	saveToFile(analysisName.c_str(), histoName, histo);
}

void TBOutput::drawAndSave(const string analysisName, const char* histoName, TNamed* histo, int dutIndex) const
{
	char histoNameWithIndex[600];
	sprintf(histoNameWithIndex, histoName, dutIndex);
	TBOutput::drawAndSave(analysisName,	histoNameWithIndex, histo);
}

void TBOutput::drawAndSave(const std::string analysisName, const char* histoName, const char* drawOpts, TNamed* histo) const{
	drawToFile(analysisName.c_str(), histoName, drawOpts, histo);
	saveToFile(analysisName.c_str(), histoName, histo);
}

char* TBOutput::buildHistName(const std::string analysisName, const char* histoName) const {
	char* fileName = new char[600];
	sprintf(fileName, "%s%s-%s-%s%s", core->tbconfig->outputPath.c_str(), core->tbconfig->rawDataName.c_str(), analysisName.c_str(),
			histoName, core->tbconfig->plotExtension.c_str());
	return fileName;
}

void TBOutput::drawToFile( const std::string analysisName, const char* histoName,
		TNamed* h1, TNamed* h2, TNamed* h3, TNamed* h4) const{
	drawToFile(analysisName.c_str(), histoName, "", h1, h2, h3, h4);
}

void TBOutput::drawToFile( const std::string analysisName, const char* histoName, const char* drawOpts,
		TNamed* h1, TNamed* h2, TNamed* h3, TNamed* h4) const{
	std::string tempExtension;
	tempExtension = core->tbconfig->plotExtension;			
	if(tempExtension!=".none") {
		char* fileName = buildHistName(analysisName.c_str(),histoName);
		TCanvas* canvas = new TCanvas();
		canvas->cd();
		std::string classname = h1->ClassName();
		if (core->tbconfig->useAtlasStyle){
			if (classname == "TH2" || classname == "TH2F" || classname == "TH2D"){
				canvas->SetRightMargin(0.15);
			}
		}
		h1->Draw(drawOpts);
		if(h2 != NULL ){
			h2->Draw("same");
		}
		if(h3 != NULL ){
			h3->Draw("same");
		}
		if(h4 != NULL ){
			h4->Draw("same");
		}
		canvas->SaveAs(fileName,"QQ");
		delete canvas;
		delete [] fileName;
	}
}

void TBOutput::drawToFile(const std::string analysisName, const char* histoName, const char* drawOpts, const std::vector<TNamed*>* hh) const {
	char* fileName = buildHistName(analysisName.c_str(),histoName);
	std::string tempExtension;
	tempExtension = core->tbconfig->plotExtension;
	if(tempExtension!=".none") {
		TCanvas* canvas = new TCanvas();
		canvas->cd();

		for(std::vector<TNamed*>::const_iterator it = hh->begin();it!=hh->end();it++){
			if(it==hh->begin()) {
				std::string classname = (*it)->ClassName();
				if (core->tbconfig->useAtlasStyle){
					if (classname == "TH2" || classname == "TH2F" || classname == "TH2D"){
						canvas->SetRightMargin(0.15);
					}
				}
				(*it)->Draw(drawOpts);
			} else {
				(*it)->Draw("SAME");
			}
		}
		canvas->SaveAs(fileName,"QQ");
		delete canvas;
		delete [] fileName;
	}
}

void TBOutput::drawToFile(const std::string analysisName, const char* histoName, const std::vector<TNamed*>* hh) const {
	drawToFile(analysisName, histoName,"",hh);
}

void TBOutput::dumpToLisp(const std::string analysisName, const char* histoName, TH1D* histo) const{
	ofstream plot;
	char* outname = new char[600];
	sprintf(outname, "%s%s-%s-%s.txt", core->tbconfig->outputPath.c_str(), core->tbconfig->rawDataName.c_str(), analysisName.c_str(), histoName);
	plot.open(outname);
	plot << "(" <<  ":x-min " << histo->GetXaxis()->GetXmin()
    		   << " :x-max " << histo->GetXaxis()->GetXmax()
    		   << " :x-step " << histo->GetBinWidth( 0 ) << ")" << std::endl;
	plot << "#(";
	for(int ii = 1; ii <= histo->GetNbinsX(); ii++){
		plot << histo->GetBinContent( ii ) << " ";
	}
	plot << ")" << std::endl;
}

char* TBOutput::getOutStreamName(const std::string analysisName, const char* streamName) const{
	char* oName = new char[800];
	sprintf(oName, "%s%s-%s-%s.txt", core->tbconfig->outputPath.c_str(), core->tbconfig->rawDataName.c_str(), analysisName.c_str(), streamName);
	return(oName);
}

/*void TBOutput::saveEventToPreprocessingFile(const Event& event, const int iden)
{
	if(TBOutput::storeevent == NULL)
	{
		TBOutput::storeevent = new StoreEvent();
		TBOutput::storeevent->hits = new TObjArray();
		TBOutput::storeevent->rawHits = new TObjArray();
		TBOutput::storeevent->clusters = new TObjArray();
		TBOutput::storeevent->rawClusters = new TObjArray();
	}
	//StoreEvent* storeevent = new StoreEvent();
	TBOutput::storeevent->iden = iden;
	// hits and rawHits
	if(event.fHits)
	{
		TBOutput::storeevent->hits->Clear();
		for(vector<PllHit*>::const_iterator it = event.hits.begin(); it != event.hits.end(); it++)
		{
			(*storeevent->hits).Add((*it));
		}
	}
	if(event.fRawHits)
	{
		storeevent->rawHits->Clear();
		for(vector<PllHit*>::const_iterator it = event.rawHits.begin(); it != event.rawHits.end(); it++)
		{
			(*storeevent->rawHits).Add((*it));
		}
	}
	storeevent->trackX = event.trackX;
	storeevent->trackY = event.trackY;
	storeevent->dxdz = event.dxdz;
	storeevent->dydz = event.dydz;
	storeevent->chi2 = event.chi2;
	storeevent->ndof = event.ndof;
	
	//global parameters
	storeevent->gr_RotXY = event.gr_RotXY;
	storeevent->gr_RotZX = event.gr_RotZX;
	storeevent->gr_RotZY = event.gr_RotZY;

	//Clusters
	if(event.fClusters)
	{
		(storeevent->clusters)->Clear();
		int i = 0, ii;
		for(vector< vector<PllHit*> >::const_iterator it = event.clusters.begin(); it != event.clusters.end(); it++)
		{
			(*storeevent->clusters).Add(new TObjArray());
			ii = 0; 
			for(vector<PllHit*>::const_iterator iit = (*it).begin(); iit != (*it).end(); iit++)
			{
				(&(*storeevent->clusters)[i])[ii] = (*iit);
				ii++;
			}
			i++;
		}
	}
	if(event.fRawClusters)
	{
		(storeevent->rawClusters)->Clear();
		int i = 0, ii;
		for(vector< vector<PllHit*> >::const_iterator it = event.rawClusters.begin(); it != event.rawClusters.end(); it++)
		{
			(*storeevent->rawClusters).Add(new TObjArray());
			ii = 0; 
			for(vector<PllHit*>::const_iterator iit = (*it).begin(); iit != (*it).end(); iit++)
			{
				(&(*storeevent->rawClusters)[i])[ii] = (*iit);
				ii++;
			}
			i++;
		}
	}
	//euClusters

	//Flags
	// A list of all the flags pertaining to an Event object and the (rough) code lines where they are potentially set. A block of several close lines may be referenced only once. Bracketed () references mean not included in the current list of eventbuilders in config.
	 storeevent->fBase = event.fBase; 
	 storeevent->fClusters = event.fClusters;
	 storeevent->fRawClusters = event.fRawClusters;
	 storeevent->fTrack = event.fTrack; 
	 storeevent->fTrackChi2 = event.fTrackChi2; 
	 storeevent->fTrackAngle = event.fTrackAngle; 
	 storeevent->fHits = event.fHits;
	 storeevent->fRawHits = event.fRawHits;
	 storeevent->fTrackCentralRegion = event.fTrackCentralRegion;  
	 storeevent->fTrackMaskedRegion = event.fTrackMaskedRegion; 
	 storeevent->fTrackRegion = event.fTrackRegion;   
	 storeevent->fTrackMatchNeighbour = event.fTrackMatchNeighbour;
	 storeevent->fEtaCorrections = event.fEtaCorrections; 
	 storeevent->fDutSync = event.fDutSync; 
	 storeevent->fSimSync = event.fSimSync; 
	 storeevent->fEtaCut = event.fEtaCut;
	
	 // save to file
	(core->tOutFilePreprocessing->GetDirectory((string(to_string(iden))).c_str()))->WriteTObject(storeevent);
	
	// TODO
	// TObjArray in storeevent->cluster und storeevent->rawCluster clearen
	// noch mal SetOwner von TObjArray angucken
	
}
*/
/*
void TBOutput::saveEventToPreprocessingFile(const Event& event, const int iden)
{
	StoreEvent* ev = new StoreEvent();
	core->tTreePreprocessing->Branch("event", "StoreEvent", &ev, 8000, 1);
	
	ev->iden = iden;
	
	ev->trackX = event.trackX;
	ev->trackY = event.trackY;
	ev->trackCol = event.trackCol;
	ev->trackRow = event.trackRow;
	ev->dxdz = event.dxdz;
	ev->dydz = event.dydz;
	ev->chi2 = event.chi2;
	ev->ndof = event.ndof;
	
	//global parameters
	ev->gr_RotXY = event.gr_RotXY;
	ev->gr_RotZX = event.gr_RotZX;
	ev->gr_RotZY = event.gr_RotZY;
	
	//Flags
	ev->fBase = event.fBase; 
	ev->fClusters = event.fClusters;
	ev->fRawClusters = event.fRawClusters;
	ev->fEdgeClusters = event.fEdgeClusters;
	ev->fTrack = event.fTrack; 
	ev->fTrackChi2 = event.fTrackChi2; 
	ev->fTrackAngle = event.fTrackAngle; 
	ev->fHits = event.fHits;
	ev->fMaskedHits = event.fMaskedHits;
	ev->fRawHits = event.fRawHits;
	ev->fTrackCentralRegion = event.fTrackCentralRegion;  
	ev->fTrackMaskedRegion = event.fTrackMaskedRegion; 
	ev->fTrackRegion = event.fTrackRegion;   
	ev->fTrackMatchNeighbour = event.fTrackMatchNeighbour;
	ev->fEtaCorrections = event.fEtaCorrections; 
	ev->fEtaCut = event.fEtaCut;
	
	core->tTreePreprocessing->Fill();
	delete ev;
}*/


TBOutput::~TBOutput()
{
}

