/* 
 * File:   HotPixelFinder.cc
 * Author: daniel
 * 
 * Created on 28. Januar 2014, 11:05
 */

#include "HotPixelFinder.h"

void HotPixelFinder::init(TBCore* core)
{
	int iden;
	int nCols;
	int nRows;
	for(auto dut: core->usedDUT)
	{
		iden = dut->iden;
		nCols = dut->getNcols();
		nRows = dut->getNrows();

		// get params
		std::string param = core->getParam(iden, HotPixelFinder::name, "maxOutOfTime");
		if(param.compare("") != 0)
		{
			HotPixelFinder::maxOutOfTime[iden] = std::fabs(std::stod(param));
		}
		else // default value
		{
			HotPixelFinder::maxOutOfTime[iden] = 5e-4;
		}
		
		HotPixelFinder::h_rawhitmap[iden] =  new TH2D("", ";Column;Row", nCols, -.5, nCols-.5, nRows, -.5, nRows-.5);
		HotPixelFinder::h_hitmap[iden] =  new TH2D("", ";Column;Row", nCols, -.5, nCols-.5, nRows, -.5, nRows-.5);
		HotPixelFinder::h_masked[iden] = new TH2D("", ";Column;Row", nCols, -.5, nCols-.5, nRows, -.5, nRows-.5);
		HotPixelFinder::h_outOfTime[iden] = new TH2D("", ";Column;Row", nCols, -.5, nCols-.5, nRows, -.5, nRows-.5);
		HotPixelFinder::h_noiseOccupancy[iden] = new TH1D("", ";;", 100, 0, HotPixelFinder::maxOutOfTime[dut->iden]);
		HotPixelFinder::h_lv1[iden] = new TH1D("", ";lv1", 16, -.5, 15.5);
		HotPixelFinder::h_allMask[iden] = new TH2I("", ";Column;Row", nCols, -.5, nCols-.5, nRows, -.5, nRows-.5);
		HotPixelFinder::eventCount[iden] = 0;

		//Extract lv1 configuration from config
		HotPixelFinder::lv1Min[iden] = dut->getlv1Min();
		HotPixelFinder::lv1Max[iden] = dut->getlv1Max();
	}
}

void HotPixelFinder::buildEvent(TBCore* core, TBEvent* event)
{
	// TODO maby set all masks in h_masked with a legend for all mask combinations
	
  // fill the right dut's histogram
   int iden = event->iden;
 
	HotPixelFinder::eventCount[iden]++;

	for(auto hit: event->rawHits)
	{
		HotPixelFinder::h_lv1[iden]->Fill(hit->lv1);
		HotPixelFinder::h_rawhitmap[iden]->Fill(hit->col, hit->row);
		if(hit->fLv1 == kBad)
		{
		  h_outOfTime[iden]->Fill(hit->col, hit->row);
		}
	}
	
	for(auto hit: event->hits)
	{
		HotPixelFinder::h_hitmap[iden]->Fill(hit->col, hit->row);
	}
}

void HotPixelFinder::finalize(TBCore* core)
{
	core->output->processName = HotPixelFinder::name;

	char* histoTitle = new char[500];
	for(auto dut: core->usedDUT)
	{
		int iden = dut->iden;

		if (dut->getMaskDeadPixels())
		{
			HotPixelFinder::checkDead(dut);
		}

		HotPixelFinder::checkNoise(dut);
		HotPixelFinder::getAllMask(dut);

		// set up cuts
		core->output->currentPreprocessCuts = "";
		// -----------
		
		std::sprintf(histoTitle, "Raw hitmap DUT %i", iden);
		HotPixelFinder::h_rawhitmap[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "rawhitmap_dut_%i", iden);
		HotPixelFinder::h_rawhitmap[iden]->SetName(histoTitle);
		core->output->drawAndSave(HotPixelFinder::h_rawhitmap[iden], "colz", "e");
		
		std::sprintf(histoTitle, "Lvl1 DUT %i", iden);
		HotPixelFinder::h_lv1[dut->iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "lv1_dut_%i", iden);
		HotPixelFinder::h_lv1[dut->iden]->SetName(histoTitle);
		core->output->drawAndSave(HotPixelFinder::h_lv1[iden], "", "e");

		// set up cuts
		core->output->currentPreprocessCuts = core->output->preprocessCuts[iden];
		// --------------
		
		std::sprintf(histoTitle, "Hitmap DUT %i", iden);
		HotPixelFinder::h_hitmap[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "hitmap_dut_%i", iden);
		HotPixelFinder::h_hitmap[iden]->SetName(histoTitle);
		core->output->drawAndSave(HotPixelFinder::h_hitmap[iden], "colz", "e");
		
		double value = HotPixelFinder::maxOutOfTime[iden];
		std::sprintf(histoTitle, "Noise occupancy DUT %i (values > %e are masked)", iden, value);
		HotPixelFinder::h_noiseOccupancy[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "noise_occupancy_dut%i", iden);
		HotPixelFinder::h_noiseOccupancy[iden]->SetName(histoTitle);
		core->output->drawAndSave(HotPixelFinder::h_noiseOccupancy[iden], "", "e");

		// set up cuts
		char* format = new char[50];
		std::sprintf(format, "%e", HotPixelFinder::maxOutOfTime[iden]);
		core->output->preprocessCuts[iden] += "; max out of time: "+std::string(format);
		core->output->currentPreprocessCuts = core->output->preprocessCuts[iden];
		// --------------
	
		std::sprintf(histoTitle, "Noise Occupancy DUT %i", iden);
		HotPixelFinder::h_outOfTime[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "outoftime_dut_%i", iden);
		HotPixelFinder::h_outOfTime[iden]->SetName(histoTitle);
		core->output->drawAndSave(HotPixelFinder::h_outOfTime[iden], "colz");

		std::sprintf(histoTitle, "Masks DUT %i", iden);
		HotPixelFinder::h_masked[iden]->SetTitle(histoTitle);
		HotPixelFinder::h_masked[iden]->GetZaxis()->SetRangeUser(0.0, 4.0);
		std::sprintf(histoTitle, "occupancy_masks_dut_%i", iden);
		HotPixelFinder::h_masked[iden]->SetName(histoTitle);
		core->output->drawAndSave(HotPixelFinder::h_masked[iden], "colz");
		
		// set up cuts
		std::sprintf(format, "%e", HotPixelFinder::maxOutOfTime[iden]);
		core->output->currentPreprocessCuts = "max out of time: "+std::string(format)+"; masked neighbour area: "+std::to_string(2*dut->getNoisyNeighborColRadius()+1)+"x"+std::to_string(2*dut->getNoisyNeighborRowRadius()+1);
		// --------------
			
		std::sprintf(histoTitle, "All Masks DUT %i", iden);
		HotPixelFinder::h_allMask[iden]->SetTitle(histoTitle);
		HotPixelFinder::h_allMask[iden]->GetZaxis()->SetRangeUser(0.0, 16.0);
		std::sprintf(histoTitle, "allMasks_dut_%i", iden);
		HotPixelFinder::h_allMask[iden]->SetName(histoTitle);
		core->output->drawAndSave(HotPixelFinder::h_allMask[iden], "colz");
		
		
		delete [] format;
		delete HotPixelFinder::h_rawhitmap[iden];
		delete HotPixelFinder::h_hitmap[iden];
		delete HotPixelFinder::h_lv1[iden];
		delete HotPixelFinder::h_masked[iden];
		delete HotPixelFinder::h_outOfTime[iden];
		delete HotPixelFinder::h_noiseOccupancy[iden];
		delete HotPixelFinder::h_allMask[iden];
	}
	HotPixelFinder::h_rawhitmap.clear();
	HotPixelFinder::h_hitmap.clear();
	HotPixelFinder::h_lv1.clear();
	HotPixelFinder::h_masked.clear();
	HotPixelFinder::h_outOfTime.clear();
	HotPixelFinder::h_noiseOccupancy.clear();
	HotPixelFinder::h_allMask.clear();
	HotPixelFinder::maxOutOfTime.clear();
	
	delete[] histoTitle;
}

void HotPixelFinder::checkDead(DUT* dut)
{
	int iden = dut->iden;
	for (int col = 1; col <= HotPixelFinder::h_hitmap[iden]->GetNbinsX(); col++) // Attention: bin count start at 1
	{
		for (int row = 1; row <= HotPixelFinder::h_hitmap[iden]->GetNbinsY(); row++) // Attention: bin count start at 1
		{
			if(HotPixelFinder::h_rawhitmap[iden]->GetBinContent(col, row) == 0)
			{
				// 4 means: pixel is dead
				HotPixelFinder::h_masked[iden]->SetBinContent(col, row, 4);
				dut->addMask(col-1, row-1, 4);  // parse bin count to dut col/row
			}
		}
	}
}

void HotPixelFinder::checkNoise(DUT* dut)
{  
	int iden = dut->iden;
	//TODO think about scaling !!!!
	HotPixelFinder::h_outOfTime[iden]->Scale(1.0 / HotPixelFinder::eventCount[iden]);
	HotPixelFinder::h_outOfTime[iden]->Scale(16.0 / (15 - (HotPixelFinder::lv1Max[iden] - HotPixelFinder::lv1Min[iden])));

	for(int col = 1; col <= HotPixelFinder::h_outOfTime[iden]->GetNbinsX(); col++) // Attention: bin count start at 1
	{
		for (int row = 1; row <= HotPixelFinder::h_outOfTime[iden]->GetNbinsY(); row++) // Attention: bin count start at 1
		{
			double val = HotPixelFinder::h_outOfTime[iden]->GetBinContent(col, row);
			if (val > 0)
			{
				HotPixelFinder::h_noiseOccupancy[iden]->Fill(val);
			}
			if (val > HotPixelFinder::maxOutOfTime[iden])
			{
				// 1 means: pixel is noisy (neighbouring pixels are masked automaticly)
				HotPixelFinder::h_masked[iden]->SetBinContent(col, row, 1);
				dut->addMask(col-1, row-1, 1); // parse bin count to dut col/row
			}
		}
	}
 }

void HotPixelFinder::getAllMask(DUT* dut)
{
	int iden = dut->iden;
	int nCols = dut->getNcols();
	int nRows = dut->getNrows();
	
	for(int col = 0; col < nCols; col++) // Attention: dut count start at 0
	{
		for(int row = 0; row < nRows; row++) // Attention: dut count start at 0
		{
			int maskValue = dut->getMask(col, row);
			HotPixelFinder::h_allMask[iden]->SetBinContent(col+1, row+1, maskValue); //parse dut col/row to bin count
		}
	}
}
