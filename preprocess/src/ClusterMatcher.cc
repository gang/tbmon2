/* 
 * File:   ClusterMatcher.cc
 * Author: daniel
 * 
 * Created on 9. Februar 2014, 22:16
 */

#include "ClusterMatcher.h"

void ClusterMatcher::buildEvent(TBCore* core, TBEvent* event)
{
	DUT* dut = event->dut;
	
	// think about: which clusters are allowed for match
	for(auto tbtrack: event->tracks)
	{
		TBCluster* tbcluster = TBCluster::getMatchedCluster(&event->clusters, tbtrack, event);
		tbtrack->matchedCluster = tbcluster;
		if(tbcluster == NULL)
		{
			tbtrack->fMatchedCluster = kBad;
		}
		else
		{
			tbtrack->fMatchedCluster = kGood;
		}
	}
}
